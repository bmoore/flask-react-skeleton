import React, {Component} from 'react';
import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import TopNav from './components/navigation/TopNav';
import PageContent from './components/page/PageContent';
import './assets/stylesheets/content.css'
import axios from 'axios';


class App extends Component {
  constructor() {
    super();
    this.state = {
        isAuth: false
    }
  }

  componentDidMount() {
    this.getAuth();
  }

  getAuth() {
    axios.get(`${process.env.REACT_APP_API_SERVICE_URL}/auth`)
    .then((res) => { 
      this.setState({isAuth: res.data["isAuth"]});
    })
    .catch((err) => {
      console.log(err);
    })
  }

  render() {
      const linkText = ['Home', 'Certificate Management']
      let isAuthenticated = this.state.isAuth;
      
    return (
      <div className="section">
        <BrowserRouter>
          <TopNav auth={isAuthenticated}/>
          <Switch>
            {/* Open routes for all users */}
            <Route exact path="/" render={(props) => <PageContent {...props} page={linkText[0]} />} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
