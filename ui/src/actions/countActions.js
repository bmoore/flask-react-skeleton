export function getCurrentExpirations(certs){
  let today = new Date();

  let monthExpireCount = 0;
  let yearExpireCount = 0;
  let alreadyExpired = 0;

  let monthExpireData = [];
  let yearExpireData = [];
  let alreadyExpiredData = [];
  let allExpireData =[]
  let currentMonth = (today.getMonth()+1);



  for (let i = 0; i < certs.length; i++){
      let expires = new Date(certs[i].expires);
      if ((today.getFullYear() === expires.getFullYear()) && (currentMonth === expires.getMonth()+1)){
        monthExpireCount++;
        monthExpireData.push(certs[i])
        allExpireData.push(certs[i])
      }
      if ((today.getFullYear() === expires.getFullYear()) && ((currentMonth !== expires.getMonth()+1) && (currentMonth < expires.getMonth()+1))){
        yearExpireCount++;
        yearExpireData.push(certs[i])
        allExpireData.push(certs[i])
      }
      if (today >= expires){
        alreadyExpired++;
        alreadyExpiredData.push(certs[i])
        allExpireData.push(certs[i])
      }
    }


let fullYearExpirations = monthExpireCount + yearExpireCount + alreadyExpired
    let data = {
      currentMonth: currentMonth,
      monthExpireCount: monthExpireCount,
      yearExpireCount: yearExpireCount,
      alreadyExpired: alreadyExpired,
      fullYearExpirations: fullYearExpirations,
      monthExpireData: monthExpireData,
      yearExpireData: yearExpireData,
      alreadyExpiredData: alreadyExpiredData,
      allExpireData: allExpireData
    }
    return data;
  }
