import React from 'react';

const CertsList = (props) => {
  return (
    <div className="container">
      <div className="columns">
      <div className="column">Connection</div>
      <div className="column">Subject DN</div>
      <div className="column">Expires</div>
      <div className="column">Contact Name</div>
      <div className="column">Contact Email</div>
      </div>
      { props.certs.map((cert) => {
        return (
          <div key={cert.id} className="columns">
          <div className="column">{ cert.connection.name }</div>
          <div className="column">{ cert.subject_dn }</div>
          <div className="column">{ cert.expires }</div>
          <div className="column">{ cert.connection.contact_first + " " + cert.connection.contact_last }</div>
          <div className="column">{ cert.connection.contact_email }</div>
          </div>
        )
      })}
    </div>
  )
};
export default CertsList;
