import React from 'react';
import {Link} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SideDrawer from './SideDrawer'

const useStyles = makeStyles(theme => ({
  root: {
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#000000',
    padding:'0 20px 0 20px'
  },
  toolBarContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'space-between'
  },
  logoContent: {
    display: 'flex',
    flexDirection: 'row'
  },
  toolbar: theme.mixins.toolbar,
}));

export default function NavBar(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolBarContent}>
          <div className={classes.logoContent}>
          <Typography variant="h6" noWrap>
          Flask React Skeleton
          </Typography>
          </div>
          <Typography noWrap>
          <Link to="/">Home</Link>
          </Typography>
          <Typography noWrap>
            Getting Started
          </Typography>
          <Typography noWrap>
            Health
          </Typography>
          <Typography noWrap>
            Support
          </Typography>
          <Typography noWrap>
            FAQ
          </Typography>
          <Typography noWrap>
            Contact
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
