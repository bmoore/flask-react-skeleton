import React from 'react';

const ConnectionsList = (props) => {
  return (
    <div className="container">
      <div className="columns">
      <div className="column">Name</div>
      <div className="column">Contact Name</div>
      <div className="column">Contact Email</div>
      </div>
      { props.connections.map((conn) => {
        return (
          <div className="columns">
          <div key={conn.id} className="column">{ conn.name }</div>
          <div key={conn.id} className="column">{ conn.contact_first + " " + conn.contact_last }</div>
          <div key={conn.id} className="column">{ conn.contact_email }</div>
          </div>
        )
      })}
    </div>
  )
};
export default ConnectionsList;
