import React from 'react';

const AddConnection = (props) => {
  return (
    <form onSubmit={(event) => props.addConnection(event)}>
      <div className="field">
        <input
          name="title"
          className="input is-large"
          type="text"
          placeholder="Enter a title"
          required
          value={props.title}
          onChange={props.handleChange}
        />
      </div>
      <div className="field">
        <input
          name="body"
          className="input is-large"
          type="text"
          placeholder="Enter a body"
          required
          value={props.body}
          onChange={props.handleChange}
        />
      </div>
      <input
        type="submit"
        className="button is-primary is-large is-fullwidth"
        value="Submit"
      />
    </form>
  )
};

export default AddConnection;
