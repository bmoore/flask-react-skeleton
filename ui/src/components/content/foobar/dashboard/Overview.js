import React, { Component } from 'react';
import PieChart from './PieChart';
import CountUps from './CountUps';
import StackedBar from './StackedBar';
import AreaChart from './AreaChart';
import axios from 'axios';
import './../../../../assets/stylesheets/cert.css'


class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      certs: [],
      name: '',
      contact_email: '',
    }
  }
  componentDidMount() {
    this.getCerts();
  }

  getCerts() {
    axios.get(`${process.env.REACT_APP_API_SERVICE_URL}/certs`)
    .then((res) => { this.setState({ certs: res.data}); })
    .catch((err) => { console.log(err); })
  }

    render() {
      let today = new Date();
      let currentMonth = (today.getMonth()+1);
      let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
      let currMonth = months[currentMonth - 1]
      return (
        <div id="dashboard-overview">
            <div className="main-chart-div">
              <PieChart certs={this.state.certs} currMonth={currMonth} />
            </div>
            <div className ="count-ups-container">
              <CountUps certs={this.state.certs} />
            </div>
            <div id="stacked-charts">
              <div>
                <StackedBar certs={this.state.certs} months={months} />
              </div>
              <div>
                <AreaChart certs={this.state.certs} months={months} />
              </div>
            </div>

        </div>
      );
    }
  }
  
  export default HomePage;