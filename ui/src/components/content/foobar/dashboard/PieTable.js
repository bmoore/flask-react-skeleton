import React, { Component } from 'react';
import './../../../../assets/stylesheets/cert.css'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import MailOutlineIcon from '@material-ui/icons/MailOutline';

class PieTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      index: 0
    }
    this.handleExpand = this.handleExpand.bind(this);
    this.populateTable = this.populateTable.bind(this);
  }
handleExpand(index){
  this.setState({index: index})
  this.state.open ? this.setState({open: false}) : this.setState({open: true});
 
}
populateTable(data){
  return data.map((obj, index) =>{
    let formattedExpires = new Date(obj.expires);
    let icon = (this.state.open && this.state.index === index) ? <ExpandMoreIcon fontSize="large" /> : <ArrowLeftIcon fontSize="large" /> 
    let selectedStyle = (this.state.open && this.state.index === index) ? "selected-row " : ""
    let extraInfo = (this.state.open && this.state.index === index) ? 

    return(
      <div>
        <div className={selectedStyle + "base-table-content"}>
          <div className="row-item">{obj.id}</div>
          <div>{expiresDate}</div>
          <div className="expand-icon" onClick={() =>{this.handleExpand(index)}}>{icon}</div>
        </div>
    {extraInfo}    
      </div>

    );

})
}

    render() {
      let data = this.props.data;
      return (
        <div id="pie-table">
                    <div id="pie-table-headers">
                        <div>ID</div>
                        <div>Expiration Date</div>
                    </div>

                <div>
                    {this.populateTable(data)}
                </div>
        </div>
      );
    }
  }
  
  export default PieTable;
