import React, { PureComponent } from 'react';
import {
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';


export default class Example extends PureComponent {

  render() {
    let today = new Date();
    let data = [
      {
        name: this.props.months[0], count: 0
      },
      {
        name: this.props.months[1], count: 0
      },
      {
        name: this.props.months[2], count: 0
      },
      {
        name: this.props.months[3], count: 0
      },
      {
        name: this.props.months[4], count: 0
      },
      {
        name: this.props.months[5], count: 0
      },
      {
        name: this.props.months[6], count: 0
      },
      {
        name: this.props.months[7], count: 0
      },
      {
        name: this.props.months[8], count: 0
      },
      {
        name: this.props.months[9], count: 0
      },
      {
        name: this.props.months[10], count: 0
      },
      {
        name: this.props.months[11], count: 0
      },
    ];
    for(let i = 0; i < this.props.certs.length; i++){
        for(let j = 0; j < this.props.certs[i].connections.length; j++){
            let expires = new Date(this.props.certs[i].expires)
            if(expires.getYear() === today.getYear()){
                switch(expires.getMonth()){
                    case 0:
                        data[0].count++
                        break;
                    case 1:
                        data[1].count++
                        break;
                    case 2:
                        data[2].count++
                        break;
                    case 3:
                        data[3].count++
                        break;
                    case 4:
                        data[4].count++
                        break;
                    case 5:
                        data[5].count++
                        break;
                    case 6:
                        data[6].count++
                        break;
                    case 7:
                        data[7].count++
                        break;
                    case 8:
                        data[8].count++
                        break;
                    case 9:
                        data[9].count++
                        break;
                    case 10:
                        data[10].count++
                        break;
                    case 11:
                        data[11].count++
                        break;
                    default:
                }
            }
        }
    }

    return (
    <div className="pie-chart">
        <div className="chart-title"><h2>Connections with Expiring Certificates</h2></div>
            <div className="chart-layout">
                <AreaChart
                    width={500}
                    height={400}
                    data={data}
                    margin={{
                    top: 10, right: 30, left: 0, bottom: 0,
                    }}
                >
                    <XAxis dataKey="name" />
                    <YAxis type="number" domain={[0, 50]} />
                    <Tooltip />
                    <Area type="monotone" dataKey="count" stroke="#06748C" fill="#28A3AF" />
                </AreaChart>
             </div>
      </div>
    );
  }
}
