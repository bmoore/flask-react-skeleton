import React, { Component } from 'react';
import CountUp from 'react-countup';
import { getCurrentExpirations } from './../../../../actions/countActions'
import './../../../../assets/stylesheets/cert.css'


class HomePage extends Component {
  constructor() {
    super();
  }


    render() {
      let today = new Date();
      let expiredCount = 0;
      let expiringApps = 0;
      let appCount = 0;
      let certs = this.props.certs;
      let expiringData = getCurrentExpirations(this.props.certs)
      for (let i = 0; i < certs.length; i++){
        let expires = new Date(certs[i].expires)
        if ((today.getFullYear() === expires.getFullYear()) && ((today.getMonth()+1) === (expires.getMonth()+1))){
          for(let j = 0; j < certs[i].connections.length; j++){
            expiringApps++;
          }
        }
        for(let j = 0; j < certs[i].connections.length; j++){
            appCount++
          }
      }

      return (
        <div id="all-counters">
            <div className="counter">
              <CountUp end={expiringData.alreadyExpired} duration={4} className="counter-num" />
              <h1 className="counter-title">Expired Certificates</h1>
            </div>
            <div className="counter">
              <CountUp end={expiringApps} duration={4} className="counter-num" />
              <h1 className="counter-title">Connections with Expiring Certificates this Month</h1>
            </div>
            <div className="counter">
              <CountUp end={certs.length} duration={4} className="counter-num" />
              <h1 className="counter-title">All Certificates</h1>
            </div>
            <div className="counter">
              <CountUp end={appCount} duration={4} className="counter-num" />
              <h1 className="counter-title">All Connections</h1>
            </div>
        </div>
      );
    }
  }
  
  export default HomePage;
