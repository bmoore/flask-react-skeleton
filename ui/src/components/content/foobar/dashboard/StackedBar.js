import React, { PureComponent } from 'react';
import { getCurrentExpirations } from './../../../../actions/countActions'
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';





export default class Example extends PureComponent {

  render() {
    let today = new Date();
    let data = [
      {
        name: this.props.months[0], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[1], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[2], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[3], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[4], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[5], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[6], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[7], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[8], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[9], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[10], signing: 0, verification: 0, encryption: 0
      },
      {
        name: this.props.months[11], signing: 0, verification: 0, encryption: 0
      },
    ];
    for(let i = 0; i < this.props.certs.length; i++){
      let expires = new Date(this.props.certs[i].expires)
      if(expires.getYear() === today.getYear()){
        switch(expires.getMonth()){
          case 0:
              if(this.props.certs[i].cert_type === "verification"){
                data[0].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[0].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[0].signing++
              }
            break;
          case 1:
              if(this.props.certs[i].cert_type === "verification"){
                data[1].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[1].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[1].signing++
              }
            break;
          case 2:
              if(this.props.certs[i].cert_type === "verification"){
                data[2].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[2].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[2].signing++
              }
            break;
          case 3:
              if(this.props.certs[i].cert_type === "verification"){
                data[3].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[3].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[3].signing++
              }
            break;
          case 4:
              if(this.props.certs[i].cert_type === "verification"){
                data[4].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[4].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[4].signing++
              }
            break;
          case 5:
              if(this.props.certs[i].cert_type === "verification"){
                data[5].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[5].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[5].signing++
              }
            break;
          case 6:
              if(this.props.certs[i].cert_type === "verification"){
                data[6].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[6].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[6].signing++
              }
            break;
          case 7:
              if(this.props.certs[i].cert_type === "verification"){
                data[7].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[7].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[7].signing++
              }
            break;
          case 8:
              if(this.props.certs[i].cert_type === "verification"){
                data[8].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[8].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[8].signing++
              }
            break;
          case 9:
              if(this.props.certs[i].cert_type === "verification"){
                data[9].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[9].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[9].signing++
              }
            break;
          case 10:
              if(this.props.certs[i].cert_type === "verification"){
                data[10].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[10].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[10].signing++
              }
            break;
          case 11:
              if(this.props.certs[i].cert_type === "verification"){
                data[11].verification++
              }
              if(this.props.certs[i].cert_type === "encryption"){
                data[11].encryption++
              }
              if(this.props.certs[i].cert_type === "signing"){
                data[11].signing++
              }
            break;
          default:
        }
      }   
    }

    return (
      <div className="pie-chart">
         <div className="chart-title"><h2>Expiring Certificates by Type</h2></div>
            <div className="chart-layout">
              <BarChart
                width={500}
                height={400}
                data={data}
                margin={{
                  top: 20, right: 30, left: 20, bottom: 5,
                }}
              >
                <XAxis dataKey="name" />
                <YAxis type="number" domain={[0, 20]}/>
                <Tooltip />
                <Legend />
                <Bar dataKey="signing" stackId="a" fill="#78E1E1" />
                <Bar dataKey="verification" stackId="a" fill="#1A1446" />
                <Bar dataKey="encryption" stackId="a" fill="#FFD000" />
              </BarChart>
            </div>
      </div>
    );
  }
}
