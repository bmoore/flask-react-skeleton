import React, { PureComponent } from 'react';
import { PieChart, Pie, Legend, Tooltip, Cell } from 'recharts';
import './../../../../assets/stylesheets/cert.css'
import { getCurrentExpirations } from './../../../../actions/countActions'
import PieTable from './PieTable';

// #ffa726
// #9575cd
// #4dd0e1
// #66bb6a
// #ffee58
// #ef5350
const COLORS = ['#78E1E1', '#1A1446', '#FFD000'];
export default class Example extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tableData: 3
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(index) {
    switch(index){
      case 0:
        this.setState({tableData: 0})
        break;
      case 1:
        this.setState({tableData: 1})
        break;
      case 2:
        this.setState({tableData: 2})
        break;
      default:
        this.setState({tableData: 3})
    }
  }
  render() {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
    let graphData = getCurrentExpirations(this.props.certs)
    let currMonth = this.props.currMonth
    let futureMonth = months[graphData.currentMonth] + " - " + months[11]
    let previousMonth = months[0] + " - " + months[graphData.currentMonth]
    let data = [
      { name: "Expiring "+currMonth, value: graphData.monthExpireCount },
      { name: "Expiring by EOY", value: graphData.yearExpireCount },
      { name: "Expired", value: graphData.alreadyExpired}
    ];
    let pieTable;
    switch(this.state.tableData){
      case 0:
        console.log(JSON.stringify(graphData.monthExpireData))
        pieTable = (<PieTable data={graphData.monthExpireData}/>)
        break;
      case 1:
        pieTable = (<PieTable data={graphData.yearExpireData}/>)
        break;
      case 2:
        pieTable = (<PieTable data={graphData.alreadyExpiredData}/>)
        break;
      case 3:
        pieTable = (<PieTable data={graphData.allExpireData}/>)
        break;
      default:
        pieTable = (<PieTable data={graphData.allExpireData}/>)
    }
    let today = new Date();
    return (
    <div>
          <div className="pie-chart">
            <div className="chart-title"><h2>Expiring Certificates in {today.getFullYear()}</h2></div>
            <div className="chart-layout">
            <PieChart width={400} height={400}>
                <Pie dataKey="value" isAnimationActive={true} data={data} cx={200} cy={200} paddingAngle={2} innerRadius={60} outerRadius={160} fill="#8884d8" label>
                {
                    data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} onClick={() =>{this.handleChange(index)}} className="pie-piece" />)
                }
                </Pie>
                <Tooltip />
                <Legend width={'100%'} wrapperStyle={{ bottom: -20, right: 0, backgroundColor: '#f5f5f5', border: '1px solid #d5d5d5', borderRadius: 3, lineHeight: '40px' }} />
            </PieChart>
            </div>
        </div>
        <div>
          {pieTable}
        </div>
    </div>
      
    );
  }
}
