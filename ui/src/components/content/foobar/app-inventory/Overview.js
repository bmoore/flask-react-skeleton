import React, { Component } from 'react';
import axios from 'axios';
import Table from '../data-table/Table'
import './../../../../assets/stylesheets/cert.css'
import './../../../../assets/stylesheets/content.css'

class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      foobars: [],
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getFooBars();
  }

  handleChange(event) {
    const obj = {};
    obj[event.target.name] = event.target.value;
    this.setState(obj)
  }

  getFooBars() {
    axios.get(`${process.env.REACT_APP_API_SERVICE_URL}/foobar`)
    .then((res) => { this.setState({ foobars: res.data}); })
    .catch((err) => { console.log(err); })

  }

    render() {

      return (
        <div className="main-table-div">
            <Table foobars={this.state.foobars} view="foobar-inventory"/>
        </div>
      );
    }
  }
  
  export default HomePage;
