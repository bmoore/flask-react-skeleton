import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
}));

const currencies = [
  {
    value: 'USD',
    label: '$',
  },
  {
    value: 'EUR',
    label: '€',
  },
  {
    value: 'BTC',
    label: '฿',
  },
  {
    value: 'JPY',
    label: '¥',
  },
];

export default function TextFields(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    notes: props.notes,
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });


    //add api call on blur here for notes
  };

  return (
    <form className={classes.container} noValidate autoComplete="off">
      <TextField
        id="standard-multiline-static"
        label=""
        multiline
        rows="3"
        value={values.notes}
        className={classes.textField}
        margin="dense"
        onChange={handleChange('notes')}
      />
    </form>
  );
}