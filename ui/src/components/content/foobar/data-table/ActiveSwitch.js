import React from 'react';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.active,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(){
    this.setState({checked: !this.state.checked});

    //need to add api call to update active vs inactive for app
  };
  render(){
let labelText = this.state.checked ? "Yes" : "No";

  return (
    <div>
      <FormControlLabel
          value="end"
          control={
            <Switch
              checked={this.state.checked}
              onChange={this.handleChange}
              value="checked"
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          }
          label={labelText}
          labelPlacement="end"
        />
    </div>
  );
}
}
export default Table;
