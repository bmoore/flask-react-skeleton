import React from "react";
import MaterialTable from 'material-table'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import ActiveSwitch from "./ActiveSwitch";
import Status from "./Status";
import AppModal from "./AppModal";
import TeamNotes from "./TeamNotes";
import './../../../../assets/stylesheets/foobar.css'

class Table extends React.Component {

  render() {
    let foobars = this.props.foobars;
    let tableData = [];
    let columnData = [];
    let tableTitle;
    switch(this.props.view){
      case 'app-inventory':
          for (let i = 0; i < foobars.length; i ++){
              tableData.push({
                bish: foobars[i].bish,
                bash: foobars[i].bash,
              })
          }
        columnData = [
          { title: 'Bish', field: 'bish', render: rowData => <AppModal rowData={rowData} /> },
          { title: 'Bash', field: 'bash'},
        ]
        tableTitle = "FooBar Inventory";
      break;
      default:
    }

   
    return (
      <div className="data-table">
      <MaterialTable
        columns={columnData}
        data={tableData}
        title={tableTitle}
      />
    </div>
    );
  }
}
  
export default Table;
