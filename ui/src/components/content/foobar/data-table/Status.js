import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

class Table extends React.Component {
  constructor(props) {

    super(props);
    this.state = {
      status: this.props.status,
    }
    this.handleChange = this.handleChange.bind(this);
  }

handleChange(event) {
  this.setState({status: event.target.value});

  //add api call to update status for given app
  };
render(){

  return (
    <div>
      <FormControl>
        <NativeSelect
          value={this.state.status}
          onChange={this.handleChange}
          name="age"
          inputProps={{ 'aria-label': 'status' }}
        >
          <option value="Complete">Complete</option>
          <option value="Scheduled">Scheduled</option>
          <option value="Incomplete">Incomplete</option>
        </NativeSelect>
      </FormControl>
    </div>
  );
}
}
export default Table;
