import React, { Component } from 'react';
import './../../assets/stylesheets/content.css'
import Home from '../content/Home'
import Tabs from '../navigation/Tabs'
import PageTabs from './../page/PageTabs'

class PageContent extends Component {
  constructor() {
    super();
    this.state = {
      error: ''
    }
  }
  render() {
    let currPage;
    let tabsInfo;
    let authErr;

    switch(this.props.page){
      case 'Home':
        if(this.props.location.state != null){
          authErr = this.props.location.state.authErr;
        };
        currPage =(<Home authErr={authErr}/>)
        
      break;
      case 'Certificate Management':
        tabsInfo = PageTabs.certTabs;  
        currPage =(
        <div>
          <Tabs tabsInfo={tabsInfo} />
        </div>
        )
      break;
      default:
        currPage =(<Home />)
        tabsInfo = PageTabs.defaultTabs
    }
    return (
      <div className='page-content'>
        <h1 className="page-title">{this.props.page}</h1>
        {currPage}
      </div>
    );
  }
  }
  
  export default PageContent;