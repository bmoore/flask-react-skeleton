from app import create_app
import os
from urllib.parse import urlparse
from redis import Redis
from rq import Queue, Connection
from rq.worker import HerokuWorker as Worker

app = create_app()

listen = ["high", "default", "low"]

url = urlparse(app.config.get("JOBS_REDIS_URL"))
conn = Redis(host=url.hostname, port=url.port, db=0, password=url.password)

if __name__ == "__main__":
    with Connection(conn):
        worker = Worker(map(Queue, listen))
        worker.work()
