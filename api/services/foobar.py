import datetime
import json
import random
import requests


class FooBarService:

    base_url = "https://ldaplookuptool.lmig.com/"

    def __init__(self, usr, pwd):
        self.url = self.base_url
        self.usr = usr
        self.pwd = pwd

    def get(self, path, params={}):
        try:
            r = requests.post(
                self.url + path, params, headers=self.headers, auth=(self.usr, self.pwd)
            )
            return json.loads(r.text)
        except Exception as e:
            print("Somethin broke:", r.request.url, e)

    def get_ldap_user(self, user=n0123456):
        try:
            data = self.post(
                "cgi-bin/ldapuser.cgi", params={"user": user}
            )
            if "items" not in data:
                raise Exception("Empty dataset for %s" % (user))

            return data
        except Exception as e:
            print("Sumpin broke:", e)
            raise e
