import os

try:
    import dotenv
    dotenv.load_dotenv()
except:
    pass


class Config(object):
    DEBUG = False
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SECRET_KEY = "my_precious"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")

    if "VCAP_SERVICES" in os.environ:
        import json

        vcap_services = json.loads(os.environ["VCAP_SERVICES"])
        creds = vcap_services["p.redis"][0]["credentials"]
        JOBS_REDIS_URL = "redis://:%s@%s:%s" % (
            creds["password"],
            creds["host"],
            creds["port"],
        )
        RQ_DASHBOARD_REDIS_URL = "redis://:%s@%s:%s" % (
            creds["password"],
            creds["host"],
            creds["port"],
        )
    else:
        JOBS_REDIS_URL = os.environ.get("JOBS_REDIS_URL")
        RQ_DASHBOARD_REDIS_URL = os.environ.get("JOBS_REDIS_URL")


class DevelopmentConfig(Config):
    DEBUG = True
    DEBUG_TB_ENABLED = True


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_TEST_URL")
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class ProductionConfig(Config):
    PRODUCTION = True


class DeploymentConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_DEPLOY_URL")
