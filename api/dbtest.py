# Used by entrypoint.sh to validate the database connection is available
import os
import config
import sqlalchemy as sqla

conf = getattr(config, os.getenv("APP_SETTINGS"))
db = sqla.create_engine(conf.SQLALCHEMY_DATABASE_URI)
db.connect()
print("Successful connection")
