from flask import Blueprint, render_template, abort

from jinja2 import TemplateNotFound

home_pages = Blueprint("home_pages", __name__, template_folder="templates")


@home_pages.route("/health")
def health_page():
    return "OK"


@home_pages.route("/import_foobar")
def import_page():
    from flask import current_app
    import jobs
    from urllib.parse import urlparse
    from redis import Redis
    from rq import Queue
    from rq.job import Job

    current_app.logger.info(current_app.config)
    url = urlparse(current_app.config.get("JOBS_REDIS_URL"))
    current_app.logger.info(url)
    conn = Redis(host=url.hostname, port=url.port, db=0, password=url.password)
    q = Queue(connection=conn)
    return str(
        q.enqueue_job(Job.create(jobs.demo.foo_job, timeout="1h", connection=conn))
    )
