import os
import random

from flask.cli import AppGroup

from app import create_app, db
from models.foobar import FooBar

foo_cli = AppGroup("foo")


@foo_cli.command("sync")
def sync():
    foo_job()


def foo_job():
    app = create_app()
    db.session.add(FooBarr(bish='heh', bash='hoh'))
    db.session.commit()
