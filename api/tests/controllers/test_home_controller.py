import json
import unittest

from tests.base import BaseTestCase


class TestHomeController(BaseTestCase):
    def test_home(self):
        """Ensure the main page renders appropriately."""
        response = self.client.get("/health")
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"OK", response.data)
