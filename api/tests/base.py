from flask_testing import TestCase

from app import create_app, db
from models.connection import Connection
from models.cert import Cert
from models.connection_cert import ConnectionCert
from models.tracking import Tracking


class BaseTestCase(TestCase):
    def create_app(self):
        self.app = create_app("TestingConfig")
        self.app_context = self.app.app_context()
        return self.app

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
