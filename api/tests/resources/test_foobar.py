import json
import random
import unittest

from tests.base import BaseTestCase
from models.foobar import FooBar
from app import db

version = "%x" % random.randrange(16 ** 8)


def add_foobar(conn):
    foobar = FooBar(
        bish=conn["bish"],
        bash=conn["bash"],
    )
    db.session.add(foobar)
    db.session.commit()
    return foobar


class TestFooBarService(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_foobar(self):
        """Ensure the /foobar route behaves correctly."""
        response = self.client.get("/api/v1/foobar")
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEqual([], data)

    def test_add_foobar(self):
        """Ensure a new foobar can be added to the database."""
        with self.client:
            response = self.client.post(
                "/api/v1/foobar",
                data=json.dumps(
                    {
                        "bish": "abc123",
                        "bash": "abc_123",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertEqual(1, data["id"])
            self.assertEqual("abc123", data["bish"])
            self.assertEqual("abc_123", data["bash"])

    def test_add_foobar_invalid_json(self):
        """Ensure error is thrown if the JSON object is empty"""
        response = self.client.post(
            "/api/v1/foobar", data=json.dumps({}), content_type="application/json"
        )
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 400)
        self.assertIn("Invalid payload.", data["message"])
        self.assertIn("fail", data["status"])

    def test_add_foobar_invalid_json_keys(self):
        """Ensure error is thrown when JSON object does not have a bish"""
        response = self.client.post(
            "/api/v1/foobar",
            data=json.dumps({}),
            content_type="application/json",
        )
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 400)
        self.assertIn("Invalid payload.", data["message"])
        self.assertIn("fail", data["status"])

    def test_single_foobar(self):
        """Ensure get single foobar behaves correctly."""
        foobar = add_foobar(
            {
                "bish": "abc123",
                "bash": "abc_123",
            }
        )
        with self.client:
            response = self.client.get(f"/api/v1/foobar/{foobar.id}")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual("abc123", data["bish"])
            self.assertEqual("abc_123", data["bash"])

    def test_single_foobar_no_id(self):
        """Ensure error is thrown if an id is not provided."""
        with self.client:
            response = self.client.get("/api/v1/foobar/blah")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertEqual("FooBar does not exist", data["message"])
            self.assertEqual("fail", data["status"])

    def test_single_foobar_incorrect_id(self):
        """Ensure error is thrown if an id does not exist."""
        with self.client:
            response = self.client.get("/api/v1/foobar/666")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertEqual("FooBar does not exist", data["message"])
            self.assertEqual("fail", data["status"])

    def test_all_foobar(self):
        """Ensure get all foobar behaves correctly."""
        add_foobar(
            {
                "bish": "abc123",
                "bash": "abc_123",
            }
        )
        add_foobar(
            {
                "bish": "def456",
                "bash": "def_456",
            }
        )
        with self.client:
            response = self.client.get("/api/v1/foobar")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data), 2)
            self.assertEqual("abc123", data[0]["bish"])
            self.assertEqual("abc_123", data[0]["bash"])
            self.assertEqual("def456", data[1]["bish"])
            self.assertEqual("def_456", data[1]["bash"])


if __name__ == "__main__":
    unittest.main()
