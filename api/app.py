import os
from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
cors = CORS()

from models import *


def create_app(config_name=None):
    app = Flask(__name__)
    if config_name == None:
        config_name = os.getenv("APP_SETTINGS")
    app.config.from_object("config." + config_name)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app)

    with app.app_context():

        # HTTP CONTROLLER DEFINITION
        from controllers.home import home_pages

        app.register_blueprint(home_pages)

        # API RESOURCE DEFINITION

        from resources.foobar import foobar_blueprint

        app.register_blueprint(foobar_blueprint, url_prefix="/api/v1")

        # THIRD-PARTY REGISTERS
        import rq_dashboard

        app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

    # FLASK CLI DEFINITION

    from jobs.demo import foo_cli

    app.cli.add_command(foo_cli)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    @app.cli.command("reassign_db")
    def reassign_db():
        db.session.execute("REASSIGN OWNED BY CURRENT_USER TO objectowner")
        db.session.commit()

    return app
