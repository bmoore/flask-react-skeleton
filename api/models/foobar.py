from models.base import BaseModel
from app import db, ma


class FooBar(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    bish = db.Column(db.String(128), unique=True, nullable=False)
    bash = db.Column(db.String(128), unique=True, nullable=False)


class FooBarSchema(ma.ModelSchema):
    class Meta:
        model = FooBar
