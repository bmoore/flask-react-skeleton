from app import db


class BaseModel(db.Model):
    __abstract__ = True

    def to_dict(self):
        return {
            key: value
            for (key, value) in self.__dict__.items()
            if key != "_sa_instance_state"
        }
