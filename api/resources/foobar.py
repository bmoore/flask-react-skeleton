from app import db
from flask import Blueprint, request
from flask_restful import Resource, Api
from sqlalchemy.exc import IntegrityError, DataError
from models.foobar import FooBar

foobar_schema = FooBarSchema()

foobar_blueprint = Blueprint("foobar", __name__)
api = Api(foobar_blueprint)


@api.resource("/foobar", "/foobar/<foobar_id>")
class FooBarResource(Resource):
    def get(self, foobar_id=None):
        version = CertVersion.query.order_by(-CertVersion.id).first().version
        failure = {"message": "FooBar does not exist", "status": "fail"}
        try:
            if foobar_id is None:
                d = foobar_schema.dump(FooBar.query.filter_by(version=version))
            else:
                d = FooBar.query.get(foobar_id)
                if not d:
                    return failure, 404
                d = foobar_schema.dump(d)
            return d
        except DataError:
            return failure, 404

    def post(self):
        data = request.get_json()
        d = FooBar(
            bish=data.get("bish"),
            bash=data.get("bash"),
        )
        try:
            db.session.add(d)
            db.session.commit()
        except IntegrityError:
            return {"message": "Invalid payload.", "status": "fail"}, 400
        return foobar_schema.dump(d), 201

    def put(self, foobar_id):
        data = request.get_json()
        d = FooBar(data)

        try:
            db.session.add(d)
            db.session.commit()
        except IntegrityError as exc:
            return {"error": exc.message}, 400

        return d.as_dict(), 201

    def delete(self, foobar_id):
        data = request.get_json()
        d = FooBar(data)

        try:
            db.session.add(d)
            db.session.commit()
        except IntegrityError as exc:
            return {"error": exc.message}, 400

        return d.as_dict(), 201
