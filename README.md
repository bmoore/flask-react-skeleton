Flask React Skeleton
===========

A base starter for Flask, React and Docker

## Getting Started

1. Clone this repository
2. `docker-compose up --build` spin up the docker images
3. Visit http://localhost:3733

## Testing

### Python Unit Tests

`docker-compose run --rm -e DATABASE_TEST_URL=sqlite:// -e APP_SETTINGS=TestingConfig --entrypoint "./entrypoint.sh pytest --junitxml=test-reports/report.xml --disable-warnings" api`

### Python Code Coverage

`docker-compose run --rm -e DATABASE_TEST_URL=sqlite:// -e APP_SETTINGS=TestingConfig --entrypoint "./entrypoint.sh pytest --cov=. --cov-config=.coveragerc tests --disable-warnings" api`

### React Unit Tests (Still in development)

`docker-compose exec client npm test`
